import turtle

def poly(t,x,y,size,side,color):
	t.speed(300)
	t.pencolor('#000000')
	t.fillcolor(color)
	t.begin_fill()
	for n in range (0,4):
		t.forward(size)
		t.left(360/side)
	t.end_fill()

	turtle.update()

def matrix(t,x,y,):
	# color list
	# 0 = dark red
	# 1 = light red
	# 2 = light blue
	# 3 = grey
	# 4 = white
	# 5 = black
	cl = ["#731111" , "#d61a1a" ,"#62f0f0" ,	"#ababab" ,"#ffffff" ,
			"#000000" ,	"#4b8093" ,"#7cafc2" ,"#96609e" ,"#a16946" ]
	# design design list
	mlist = ["44444455544444444",
			 "44445511155444444",
			 "44451111111544444",
			 "44555555111544444",
			 "44524422511154444",
			 "45244422351155544",
			 "45222223351151154",
			 "44533333511051154",
			 "44555555111050054",
			 "44511111110050054",
			 "44501111100050054",
			 "44500000000055544",
			 "44500555500054444",
			 "44500544450054444",
			 "44455444445544444",
			 ]
	print(t,x,y)
	t.width(1)
	for k in range(0,15): #height
		for h in range(0,17): #width
			colorstring = mlist[k][h]
			colorint = int(colorstring)
			color = cl[colorint]
			print(x*20,y*20)
			print(" color ",color," ",end="")
			t.penup()
			t.goto(h*20,k*-20)
			t.pendown()
			poly(t,h,k,20,4,color)
			


def johnny():
	turtle.TurtleScreen._RUNNING = True
	turtle.screensize(canvwidth=1000, canvheight=1000, bg=None)
	x = -400; y = 0
	w = turtle.Screen()
	w.clear()
	w.bgcolor("#ffffff")
	t = turtle.Turtle()

	turtle.tracer(0, 0)
	
	matrix(t,x,y)
	
	w.exitonclick()
	
if __name__ == '__main__':
	johnny()

'''
cl = ["#f8f8f8","#e8e8e8" ,"#d8d8d8" ,"#b8b8b8" ,
				"#585858" ,"#383838" ,"#282828" , "#181818" ,
				"#ab4642" ,	"#dc9656" ,"#f79a0e" ,"#538947" ,
				"#4b8093" ,"#7cafc2" ,"#96609e" ,"#a16946" ]

'''
