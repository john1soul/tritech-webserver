import tkinter as tk
from Enderman import *
from Ryland import *
from wave import *
from Joker import *
'''
#f8f8f8   #e8e8e8 #d8d8d8 #b8b8b8   #585858 #383838 #282828 #181818
#ab4642 #dc9656 #f79a0e #538947 #4b8093   #7cafc2 #96609e #a16946
'''

class MainMenu:
    def __init__(self, master,*args,**kwargs):
        self.master = master
        self.master.minsize(700, 700)
        self.master.wm_title(". . . PROJECT MENU . . .")
        self.master.option_add("*Font", "helvetica 24")
        # start frame
        self.frame = tk.Frame(self.master, relief='raised', borderwidth=5, background="#000000")
        self.frame.place()
        #button 1
        self.button1 = tk.Button(self.frame, text = 'Enderman', width = 25, command = johnny)
        self.button1.pack()
        #button 2
        self.button2 = tk.Button(self.frame, text = 'Joker', width = 25, command = joey)
        self.button2.pack()
        #button 3
        self.button3 = tk.Button(self.frame, text = 'Wave', width = 25, command = jacob)
        self.button3.pack()
        #button 4
        self.button4 = tk.Button(self.frame, text = 'Fire', width = 25, command = ryland)
        self.button4.pack()
        # end of button placement
        self.quitButton = tk.Button(self.frame, text = 'Quit', width = 25, command = self.close_windows)
        self.quitButton.pack()
        self.text = tk.Label(self.frame, text="* * * * * * * * * * * * * \n \
JJJR Python Project:  \n  \
{The Items Have Been Designed By The Masters of Time Space and Dimension} \n \
* * * * * * * * * * * * * \n  \
The JJJR Foundations Individual Projects \n \
{Created by many talented people} \n \
* * * * * * * * * * * * * \n \
Update 011221 (Credit to Craig Coleman)  \n \
J. Smith ",font=('courier', '24'),foreground="#000000" ,background="#B559B5" )
        self.text.pack()
        self.frame.pack(expand=True, fill='both')
        # labels can be text or images
        # * * * * * * * * *

    def new_window(self):
        self.newWindow = tk.Toplevel(self.master)
        self.app = ChoasWindow(self.newWindow)
    def close_windows(self):
        self.master.destroy()

class ChoasWindow:
    def __init__(self, master):
        self.master = master
        self.frame = tk.Frame(self.master)
        self.quitButton = tk.Button(self.frame, text = 'Quit', width = 25, command = self.close_windows)
        self.quitButton.pack()
        self.frame.pack()
    def close_windows(self):
        self.master.destroy()

def main():
    root = tk.Tk()
    import sys
    print(sys.version)
    app = MainMenu(root)
    root.mainloop()

if __name__ == '__main__':
    main()


"""
apt install python3-pil python-pil python3-pil.imagetk python-pil.imagetk;
apt install python3-pil python-pil
apt install python3-pil.imagetk
apt install python-pil.imagetk

https://pythonprogramming.net/tkinter-adding-text-images/
https://stackoverflow.com/questions/17466561/best-way-to-structure-a-tkinter-application
https://stackoverflow.com/questions/23901168/how-do-i-insert-a-jpeg-image-into-a-python-tkinter-window
$base03:    #002b36; (dark grey)
$base02:    #073642;
$base01:    #586e75;
$base00:    #657b83;
$base0:     #839496;
$base1:     #93a1a1;
$base2:     #eee8d5; (cream)
$base3:     #fdf6e3;
$yellow:    #b58900;
$orange:    #cb4b16;
$red:       #dc322f;
$magenta:   #d33682;
$violet:    #6c71c4;
$blue:      #268bd2;
$cyan:      #2aa198;
$green:     #859900;

"""
